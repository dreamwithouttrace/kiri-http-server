<?php
declare(strict_types=1);

namespace Kiri\Server;


use Exception;
use Kiri;
use Kiri\Server\Events\OnWorkerStart;
use Kiri\Events\EventProvider;
use Kiri\Events\EventDispatch;
use Kiri\Router\Router;
use Kiri\Server\Abstracts\AsyncServer;
use Kiri\Server\Events\OnShutdown;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use function config;
use Kiri\Server\Abstracts\HotReload;
use Kiri\Di\Inject\Container;

defined('ROUTER_TYPE_HTTP') or define('ROUTER_TYPE_HTTP', 'http');
defined('PID_PATH') or define('PID_PATH', APP_PATH . 'storage/server.pid');

/**
 * Class Command
 * @package Http
 */
class ServerCommand extends Command
{


    public State $state;


    /**
     * @var ContainerInterface
     */
    #[Container(ContainerInterface::class)]
    public ContainerInterface $container;


    /**
     * @var AsyncServer
     */
    #[Container(AsyncServer::class)]
    public AsyncServer $asyncServer;


    /**
     * @var EventDispatch
     */
    #[Container(EventDispatch::class)]
    public EventDispatch $eventDispatch;


    /**
     * @var EventProvider
     */
    #[Container(EventProvider::class)]
    public EventProvider $eventProvider;


    /**
     * @param string|null $name
     * @throws Exception
     */
    public function __construct(string $name = null)
    {
        parent::__construct($name);
        $this->state = Kiri::getDi()->get(State::class);
    }


    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setName('sw:server')
             ->setDescription('server start|stop|reload|restart')
             ->addArgument('action', InputArgument::OPTIONAL, 'run action', 'start')
             ->addOption('daemon', 'd', InputOption::VALUE_NONE, 'is run daemonize');
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        return match ($input->getArgument('action')) {
            'restart' => $this->restart($input),
            'stop'    => $this->stop(),
            'start'   => $this->start($input),
            default   =>
            throw new Exception('I don\'t know what I want to do.')
        };
    }


    /**
     * @param InputInterface $input
     * @return int
     * @throws
     */
    protected function restart(InputInterface $input): int
    {
        $this->stop();
        $this->start($input);
        return 1;
    }


    /**
     * @return int
     * @throws
     */
    protected function stop(): int
    {
        $configs   = config('server', []);
        $instances = $this->asyncServer->sortService($configs['ports'] ?? []);
        foreach ($instances as $config) {
            $this->state->exit($config->port);
        }
        $this->eventDispatch->dispatch(new OnShutdown());
        return 1;
    }


    /**
     * @param InputInterface $input
     * @return int
     * @throws
     */
    protected function start(InputInterface $input): int
    {
        $this->asyncServer->addProcess(config('processes', []));
        if (\config('reload.hot', false) === true) {
            $this->asyncServer->addProcess([HotReload::class]);
        } else {
            di(Router::class)->scan_build_route();
        }
        $this->asyncServer->initCoreServers(config('server', []), (int)$input->getOption('daemon'));
        $this->asyncServer->start();
        return 1;
    }

}
