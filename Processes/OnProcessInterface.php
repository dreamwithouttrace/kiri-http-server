<?php


namespace Kiri\Server\Processes;


use Swoole\Process;


/**
 * Interface AbstractProcess
 * @package Contract
 */
interface OnProcessInterface
{


	/**
	 * @param ?Process $process
	 */
	public function process(?Process $process): void;



}
