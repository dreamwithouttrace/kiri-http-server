<?php

namespace Kiri\Server\Abstracts;

use Kiri\Di\Inject\Container;
use Kiri\Error\StdoutLogger;
use Kiri\Events\EventProvider;
use Kiri\Router\Router;
use Kiri\Server\Events\OnWorkerStart;
use Kiri\Server\ServerInterface;
use Psr\Log\LoggerInterface;
use Swoole\Event;
use Swoole\Process;
use Kiri\Server\Processes\AbstractProcess;

class HotReload extends AbstractProcess
{


    /**
     * @var mixed
     */
    protected mixed $pipe;


    /**
     * @var LoggerInterface|StdoutLogger
     */
    #[Container(LoggerInterface::class)]
    public StdoutLogger|LoggerInterface $logger;


    /**
     * @var bool
     */
    protected bool $enable_coroutine = false;


    /**
     * @var array
     */
    protected array $watches = [];


    /**
     * @var bool
     */
    protected bool $enable_queue = false;


    /**
     * @var bool
     */
    protected bool $reloading = false;


    /**
     * @throws \Exception
     */
    public function __construct()
    {
        di(EventProvider::class)->on(OnWorkerStart::class, [di(Router::class), 'scan_build_route']);
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return 'hotReload';
    }


    /**
     * @return void
     */
    public function onSigterm(): void
    {
        // TODO: Implement onSigterm() method.
        $this->stop();
    }

    /**
     * @param ?Process $process
     */
    public function process(Process|null $process): void
    {
        $this->pipe = inotify_init();
        $this->addListen();
        Event::add($this->pipe, function () use ($process) {
            $read = inotify_read($this->pipe);
            if (count($read) > 0) {
                $this->reload();
            }
        });
        Event::cycle(function (): void {
            if ($this->isStop()) {
                Event::exit();
            }
        });
        Event::wait();
    }


    /**
     * @return void
     */
    public function reload(): void
    {
        if ($this->reloading) {
            return;
        }
        $this->reloading = true;

        di(StdoutLogger::class)->println('reloading server[' . \config('id', 'system-service') . '], please waite.');

        $this->clear();
        di(ServerInterface::class)->reload();
        $this->addListen();

        $this->reloading = false;
    }


    /**
     * @return void
     */
    protected function addListen(): void
    {
        foreach (config('reload.listen') as $value) {
            $this->readDirectory($value);
        }
    }


    /**
     * @return void
     */
    protected function clear(): void
    {
        $this->watches = [];
    }

    /**
     * @param string $directory
     * @return void
     */
    public function readFile(string $directory): void
    {
        if (str_ends_with($directory, '.php') === true) {
            inotify_add_watch($this->pipe, $directory, IN_MODIFY | IN_MOVE | IN_CREATE | IN_DELETE);
        }
    }


    /**
     * @param string $directory
     * @return void
     */
    public function readDirectory(string $directory): void
    {
        foreach (glob($directory . '/*') as $data) {
            if (is_dir($data)) {
                $this->readDirectory($data);
            } else {
                $this->readFile($data);
            }
        }
    }
}