<?php

namespace Kiri\Server\Abstracts;

use Exception;
use Kiri;
use Swoole\Http\Server as HServer;
use Swoole\Server;
use Kiri\Server\Processes\AbstractProcess;
use Kiri\Server\Constant;
use Kiri\Server\Config;
use Swoole\WebSocket\Server as WServer;
use Swoole\Process;

trait TraitServer
{

    /**
     * @return void
     * @throws
     */
    public function onSignal(): void
    {
        $signal = \config('signal', []);
        $this->onPcntlSignal(SIGINT, [$this, 'onSigint']);
        foreach ($signal as $sig => $value) {
            if (is_array($value) && is_string($value[0])) {
                $value[0] = \Kiri::getDi()->get($value[0]);
            }
            if (!is_callable($value, true)) {
                throw new Exception('Register signal callback must can exec.');
            }
            $this->onPcntlSignal($sig, $value);
        }
    }


    /**
     * @param $no
     * @param array $signInfo
     * @return void
     */
    public function onSigint($no, array $signInfo): void
    {
        try {
            Kiri::getLogger()->alert('Pid ' . getmypid() . ' get signo ' . $no);
            $this->shutdown();
        } catch (\Throwable $exception) {
            error($exception);
        }
    }


    /**
     * @param $signal
     * @param $callback
     * @return void
     */
    private function onPcntlSignal($signal, $callback): void
    {
        \pcntl_signal($signal, $callback);
    }


    /**
     * @param array $ports
     * @return array
     */
    public function sortService(array $ports): array
    {
        $array = [];
        foreach ($ports as $port) {
            $array = $this->sort($array, $port);
        }
        return $array;
    }


    /**
     * @param array $ports
     * @return array<Config>
     */
    public function genConfigService(array $ports): array
    {
        $array = [];
        $ports = $ports['ports'] ?? [];
        foreach ($ports as $port) {
            $array = $this->sort($array, $port);
        }
        return $array;
    }


    /**
     * @param array $array
     * @param $port
     * @return array
     */
    private function sort(array $array, $port): array
    {
        $config = instance(Config::class, [], $port);
        if ($port['type'] == Constant::SERVER_TYPE_WEBSOCKET) {
            array_unshift($array, $config);
        } else if ($port['type'] == Constant::SERVER_TYPE_HTTP) {
            if (!empty($array) && $array[0]['type'] == Constant::SERVER_TYPE_WEBSOCKET) {
                $array[] = $config;
            } else {
                array_unshift($array, $config);
            }
        } else {
            $array[] = $config;
        }
        return $array;
    }


    /**
     * @param $type
     * @return string|null
     */
    public function getServerClass($type): ?string
    {
        return match ($type) {
            Constant::SERVER_TYPE_BASE, Constant::SERVER_TYPE_TCP,
            Constant::SERVER_TYPE_UDP => Server::class,
            Constant::SERVER_TYPE_HTTP => HServer::class,
            Constant::SERVER_TYPE_WEBSOCKET => WServer::class,
            default => null
        };
    }

}
